<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\DeleteAction;
use App\Entity\Interfaces\CreatedAtSettableInterface;
use App\Entity\Interfaces\IsDeletedSettableInterface;
use App\Entity\Interfaces\UpdatedAtSettableInterface;
use App\Repository\TeacherRepository;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: TeacherRepository::class)]
#[ApiResource(
    collectionOperations: [
        'get',
        'post' => [
            'security' => "is_granted('ROLE_ADMIN')",
        ]
    ],
    itemOperations: [
        'get' => [
            'security' => "is_granted('ROLE_ADMIN')",
        ],
        'put' => [
            'security' => "is_granted('ROLE_ADMIN')",
        ],
        'delete' => [
            'controller' => DeleteAction::class,
            'security' => "is_granted('ROLE_ADMIN')",
        ],
    ],
    denormalizationContext: ['groups' => ['teacher:write']],
    normalizationContext: ['groups' => ['teacher:read', 'teachers:read']],
)]
class Teacher implements
    CreatedAtSettableInterface,
    UpdatedAtSettableInterface,
    IsDeletedSettableInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['teachers:read', 'courses:read'])]
    private $id;

    #[Groups(['teachers:read', 'teacher:write', 'courses:read'])]
    #[ORM\Column(type: 'string', length: 255)]
    private $givenName;

    #[Groups(['teachers:read', 'teacher:write', 'courses:read'])]
    #[ORM\Column(type: 'string', length: 255)]
    private $familyName;

    #[Groups(['teachers:read', 'teacher:write', 'courses:read'])]
    #[ORM\Column(type: 'string', length: 255)]
    private $patronymic;

    #[Groups(['teachers:read', 'teacher:write', 'courses:read'])]
    #[ORM\Column(type: 'string', length: 255)]
    private $degree;

    #[Groups(['teachers:read', 'courses:read', 'teacher:write'])]
    #[ORM\Column(type: 'string', length: 255)]
    private $experience;

    #[Groups(['teachers:read', 'courses:read', 'teacher:write'])]
    #[ORM\Column(type: 'string', length: 255)]
    private $country;

    #[Groups(['teachers:read', 'courses:read', 'teacher:write'])]
    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $additional;

    #[Groups(['teachers:read'])]
    #[ORM\Column(type: 'datetime')]
    private $createdAt;

    #[Groups(['teachers:read'])]
    #[ORM\Column(type: 'datetime', nullable: true)]
    private $updatedAt;

    #[ORM\Column(type: 'boolean')]
    private $isDeleted = false;

    #[Groups(['teachers:read'])]
    #[ORM\OneToOne(mappedBy: 'teacher', targetEntity: Course::class, cascade: ['persist', 'remove'])]
    private $course;

    #[Groups(['teachers:read', 'courses:read', 'teacher:write'])]
    #[ORM\OneToOne(targetEntity: MediaObject::class, cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private $picture;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGivenName(): ?string
    {
        return $this->givenName;
    }

    public function setGivenName(string $givenName): self
    {
        $this->givenName = $givenName;

        return $this;
    }

    public function getFamilyName(): ?string
    {
        return $this->familyName;
    }

    public function setFamilyName(string $familyName): self
    {
        $this->familyName = $familyName;

        return $this;
    }

    public function getPatronymic(): ?string
    {
        return $this->patronymic;
    }

    public function setPatronymic(string $patronymic): self
    {
        $this->patronymic = $patronymic;

        return $this;
    }

    public function getDegree(): ?string
    {
        return $this->degree;
    }

    public function setDegree(string $degree): self
    {
        $this->degree = $degree;

        return $this;
    }

    public function getExperience(): ?string
    {
        return $this->experience;
    }

    public function setExperience(string $experience): self
    {
        $this->experience = $experience;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getAdditional(): ?string
    {
        return $this->additional;
    }

    public function setAdditional(?string $additional): self
    {
        $this->additional = $additional;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function isIsDeleted(): ?bool
    {
        return $this->isDeleted;
    }

    public function setIsDeleted(bool $isDeleted): self
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    public function getCourse(): ?Course
    {
        return $this->course;
    }

    public function setCourse(Course $course): self
    {
        // set the owning side of the relation if necessary
        if ($course->getTeacher() !== $this) {
            $course->setTeacher($this);
        }

        $this->course = $course;

        return $this;
    }

    public function getPicture(): ?MediaObject
    {
        return $this->picture;
    }

    public function setPicture(MediaObject $picture): self
    {
        $this->picture = $picture;

        return $this;
    }
}
