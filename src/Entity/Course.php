<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\DeleteAction;
use App\Controller\UserApplyToCourseAction;
use App\Entity\Interfaces\CreatedAtSettableInterface;
use App\Entity\Interfaces\IsDeletedSettableInterface;
use App\Entity\Interfaces\UpdatedAtSettableInterface;
use App\Repository\CourseRepository;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: CourseRepository::class)]
#[ApiResource(
    collectionOperations: [
        'get',
        'post' => [
            'security' => "is_granted('ROLE_ADMIN')",
        ]
    ],
    itemOperations: [
        'apply_course' => [
            'controller' => UserApplyToCourseAction::class,
            'method' => 'get',
            'path' => 'courses/apply/{id}',
        ],
        'get' => [
            'security' => "is_granted('ROLE_ADMIN')",
        ],
        'put' => [
            'security' => "is_granted('ROLE_ADMIN')",
        ],
        'delete' => [
            'controller' => DeleteAction::class,
            'security' => "is_granted('ROLE_ADMIN')",
        ],
    ],
    denormalizationContext: ['groups' => ['course:write']],
    normalizationContext: ['groups' => ['course:read', 'courses:read']],
)]
class Course implements
    CreatedAtSettableInterface,
    UpdatedAtSettableInterface,
    IsDeletedSettableInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['courses:read'])]
    private $id;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['courses:read'])]
    private $createdAt;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['courses:read'])]
    private $updatedAt;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['courses:read', 'course:write', 'course:put:write'])]
    private $isActive = true;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['courses:read', 'course:write', 'course:put:write'])]
    private $price;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['courses:read', 'course:write', 'course:put:write'])]
    private $name;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['courses:read', 'course:write', 'course:put:write'])]
    private $members;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['courses:read', 'course:write', 'course:put:write'])]
    private $countOfStudents;

    #[ORM\Column(type: 'boolean')]
    private $isDeleted = false;

    #[ORM\OneToOne(inversedBy: 'course', targetEntity: Teacher::class, cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['courses:read', 'course:write', 'course:put:write'])]
    private $teacher;

    #[ORM\ManyToMany(targetEntity: User::class, mappedBy: 'courses')]
    #[Groups(['courses:read', 'course:write', 'course:put:write'])]
    private $students;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['courses:read', 'course:write', 'course:put:write'])]
    private $startingDate;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['courses:read', 'course:write', 'course:put:write'])]
    private $additionalInfo;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['courses:read', 'course:write', 'course:put:write'])]
    private $futureSkills;

    public function __construct()
    {
        $this->students = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function isIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMembers(): ?string
    {
        return $this->members;
    }

    public function setMembers(string $members): self
    {
        $this->members = $members;

        return $this;
    }

    public function getCountOfStudents(): ?string
    {
        return $this->countOfStudents;
    }

    public function setCountOfStudents(string $countOfStudents): self
    {
        $this->countOfStudents = $countOfStudents;

        return $this;
    }

    public function isIsDeleted(): ?bool
    {
        return $this->isDeleted;
    }

    public function setIsDeleted(bool $isDeleted): self
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    public function getTeacher(): ?Teacher
    {
        return $this->teacher;
    }

    public function setTeacher(Teacher $teacher): self
    {
        $this->teacher = $teacher;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getStudents(): Collection
    {
        return $this->students;
    }

    public function addStudent(User $student): self
    {
        if (!$this->students->contains($student)) {
            $this->students[] = $student;
            $student->addCourse($this);
        }

        return $this;
    }

    public function removeStudent(User $student): self
    {
        if ($this->students->removeElement($student)) {
            $student->removeCourse($this);
        }

        return $this;
    }

    public function getStartingDate(): ?DateTimeInterface
    {
        return $this->startingDate;
    }

    public function setStartingDate(DateTimeInterface $startingDate): self
    {
        $this->startingDate = $startingDate;

        return $this;
    }

    public function getAdditionalInfo(): ?string
    {
        return $this->additionalInfo;
    }

    public function setAdditionalInfo(string $additionalInfo): self
    {
        $this->additionalInfo = $additionalInfo;

        return $this;
    }

    public function getFutureSkills(): ?string
    {
        return $this->futureSkills;
    }

    public function setFutureSkills(string $futureSkills): self
    {
        $this->futureSkills = $futureSkills;

        return $this;
    }
}
