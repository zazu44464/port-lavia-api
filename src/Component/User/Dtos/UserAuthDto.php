<?php

declare(strict_types=1);

namespace App\Component\User\Dtos;

use Symfony\Component\Validator\Constraints as Assert;

class UserAuthDto
{
    /**
     * @Assert\Email()
     * @Assert\NotBlank()
     */
    private string $email;

    /**
     * @Assert\NotBlank()
     */
    private string $password;

    private bool $isManager;

    public function setIsManager(bool $isManager): void
    {
        $this->isManager = $isManager;
    }

    public function isManager(): bool
    {
        return $this->isManager;
    }

    public function __construct(string $email, string $password)
    {
        $this->email = $email;
        $this->password = $password;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPassword(): string
    {
        return $this->password;
    }
}
