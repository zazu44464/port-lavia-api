<?php
declare(strict_types=1);

namespace App\Component\User\Dtos;

use App\Entity\Course;
use Symfony\Component\Validator\Constraints as Assert;

class UserDto
{
    /**
     * @Assert\Email()
     * @Assert\NotBlank()
     */
    private string $email;

    /**
     * @Assert\Length(min="6")
     */
    private string $password;

    /**
     * @Assert\NotBlank()
     */
    private string $telephone;

    /**
     * @Assert\NotBlank()
     */
    private string $givenName;

    /**
     * @Assert\NotBlank()
     */
    private string $familyName;

    /**
     * @Assert\NotBlank()
     */
    private string $patronymic;

    /**
     * @Assert\NotBlank()
     */
    private string $telegramNumber;

    /**
     * @Assert\NotBlank()
     */
    private int $age;

    /**
     * @Assert\NotBlank()
     */
    private string $address;

    public function __construct(
        string $email,
        string $password,
        string $givenName,
        string $familyName,
        string $patronymic,
        string $telephone,
        string $telegramNumber,
        int    $age,
        string $address,
    )
    {
        $this->email = $email;
        $this->password = $password;
        $this->age = $age;
        $this->givenName = $givenName;
        $this->familyName = $familyName;
        $this->patronymic = $patronymic;
        $this->address = $address;
        $this->telegramNumber = $telegramNumber;
        $this->telephone = $telephone;
    }

    /**
     * @return string
     */
    public function getTelephone(): string
    {
        return $this->telephone;
    }

    /**
     * @return string
     */
    public function getGivenName(): string
    {
        return $this->givenName;
    }

    /**
     * @return string
     */
    public function getFamilyName(): string
    {
        return $this->familyName;
    }

    /**
     * @return string
     */
    public function getPatronymic(): string
    {
        return $this->patronymic;
    }

    /**
     * @return string
     */
    public function getTelegramNumber(): string
    {
        return $this->telegramNumber;
    }

    /**
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPassword(): string
    {
        return $this->password;
    }
}
