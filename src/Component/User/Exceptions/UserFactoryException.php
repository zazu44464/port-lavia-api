<?php declare(strict_types=1);

namespace App\Component\User\Exceptions;

use Exception;

/**
 * Class UserFactoryException
 *
 * @package App\Component\User\Exceptions
 */
class UserFactoryException extends Exception
{
}
