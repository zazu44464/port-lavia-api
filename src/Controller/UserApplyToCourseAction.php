<?php

namespace App\Controller;

use App\Component\Core\ParameterGetter;
use App\Component\User\CurrentUser;
use App\Controller\Base\AbstractController;
use App\Entity\Course;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;

class UserApplyToCourseAction extends AbstractController
{
    /**
     * @throws NotFoundExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ContainerExceptionInterface
     */
    public function __invoke(
        Course          $data,
        CurrentUser     $currentUser,
        ParameterGetter $parameterGetter,
        MailerInterface $mailer,
    ): Response
    {
        $user = $currentUser->getUser();

        $email = (new TemplatedEmail())
            ->from(
                new Address($parameterGetter->get('mailer_address'), $parameterGetter->get('mailer_name'))
            )
            ->to($parameterGetter->get('operator_address'))
            ->subject('Yangi arizachi')
            ->htmlTemplate('email/email.html.twig')
            ->context([
                'name' => $user->getFamilyName() . ' ' . $user->getGivenName() . ' ' . $user->getPatronymic(),
                'age' => $user->getAge(),
                'email_address' => $user->getEmail(),
                'telephone' => $user->getTelephone(),
                'telegramNumber' => $user->getTelegramNumber(),
                'address' => $user->getAddress(),
                'courseName' => $data->getName(),
                'isNew' => false
            ]);

        $mailer->send($email);

        return $this->responseNormalized(
            'success'
        );
    }
}