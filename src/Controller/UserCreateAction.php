<?php

declare(strict_types=1);

namespace App\Controller;

use App\Component\Core\ParameterGetter;
use App\Component\User\Dtos\UserDto;
use App\Component\User\Exceptions\UserFactoryException;
use App\Component\User\UserMaker;
use App\Controller\Base\AbstractController;
use App\Entity\User;
use Psr\Container\ContainerExceptionInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;

/**
 * Class CreateUserController
 *
 * @method UserDto getDtoFromRequest(Request $request, string $dtoClass)
 *
 * @package App\Controller
 */
class UserCreateAction extends AbstractController
{
    public function __invoke(
        Request         $request,
        UserMaker       $userMaker,
        ParameterGetter $parameterGetter,
        MailerInterface $mailer
    ): User
    {
        $userDto = $this->getDtoFromRequest($request, UserDto::class);
        $this->validate($userDto);

        try {
            $user = $userMaker->make($userDto);

            $email = (new TemplatedEmail())
                ->from(
                    new Address($parameterGetter->get('mailer_address'), $parameterGetter->get('mailer_name'))
                )
                ->to($parameterGetter->get('operator_address'))
                ->subject('Yangi foydalanuvchi')
                ->htmlTemplate('email/email.html.twig')
                ->context([
                    'name' => $user->getFamilyName() . ' ' . $user->getGivenName() . ' ' . $user->getPatronymic(),
                    'age' => $user->getAge(),
                    'email_address' => $user->getEmail(),
                    'telephone' => $user->getTelephone(),
                    'telegramNumber' => $user->getTelegramNumber(),
                    'address' => $user->getAddress(),
                    'courseName' => '',
                    'isNew' => true
                ]);

            $mailer->send($email);

            return $user;

        } catch (UserFactoryException $e) {
            throw new BadRequestHttpException($e->getMessage());
        } catch (TransportExceptionInterface|ContainerExceptionInterface $e) {
            throw new BadRequestHttpException($e->getMessage());
        }


    }
}
