<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221111112744 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE about_us (id INT AUTO_INCREMENT NOT NULL, text VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, is_deleted TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE about_us_media_object (about_us_id INT NOT NULL, media_object_id INT NOT NULL, INDEX IDX_EE9DF0B27CE2CF2D (about_us_id), INDEX IDX_EE9DF0B264DE5A5 (media_object_id), PRIMARY KEY(about_us_id, media_object_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE course (id INT AUTO_INCREMENT NOT NULL, teacher_id INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, is_active TINYINT(1) NOT NULL, price VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, members VARCHAR(255) NOT NULL, count_of_students VARCHAR(255) NOT NULL, is_deleted TINYINT(1) NOT NULL, starting_date DATETIME NOT NULL, additional_info VARCHAR(255) NOT NULL, future_skills VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_169E6FB941807E1D (teacher_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE media_object (id INT AUTO_INCREMENT NOT NULL, file_path VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE teacher (id INT AUTO_INCREMENT NOT NULL, picture_id INT NOT NULL, given_name VARCHAR(255) NOT NULL, family_name VARCHAR(255) NOT NULL, patronymic VARCHAR(255) NOT NULL, degree VARCHAR(255) NOT NULL, experience VARCHAR(255) NOT NULL, country VARCHAR(255) NOT NULL, additional VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, is_deleted TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_B0F6A6D5EE45BDBF (picture_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_course (user_id INT NOT NULL, course_id INT NOT NULL, INDEX IDX_73CC7484A76ED395 (user_id), INDEX IDX_73CC7484591CC992 (course_id), PRIMARY KEY(user_id, course_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE about_us_media_object ADD CONSTRAINT FK_EE9DF0B27CE2CF2D FOREIGN KEY (about_us_id) REFERENCES about_us (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE about_us_media_object ADD CONSTRAINT FK_EE9DF0B264DE5A5 FOREIGN KEY (media_object_id) REFERENCES media_object (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE course ADD CONSTRAINT FK_169E6FB941807E1D FOREIGN KEY (teacher_id) REFERENCES teacher (id)');
        $this->addSql('ALTER TABLE teacher ADD CONSTRAINT FK_B0F6A6D5EE45BDBF FOREIGN KEY (picture_id) REFERENCES media_object (id)');
        $this->addSql('ALTER TABLE user_course ADD CONSTRAINT FK_73CC7484A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_course ADD CONSTRAINT FK_73CC7484591CC992 FOREIGN KEY (course_id) REFERENCES course (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user ADD telephone VARCHAR(255) NOT NULL, ADD given_name VARCHAR(255) NOT NULL, ADD family_name VARCHAR(255) NOT NULL, ADD patronymic VARCHAR(255) NOT NULL, ADD telegram_number VARCHAR(255) NOT NULL, ADD age INT NOT NULL, ADD address VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE about_us_media_object DROP FOREIGN KEY FK_EE9DF0B27CE2CF2D');
        $this->addSql('ALTER TABLE user_course DROP FOREIGN KEY FK_73CC7484591CC992');
        $this->addSql('ALTER TABLE about_us_media_object DROP FOREIGN KEY FK_EE9DF0B264DE5A5');
        $this->addSql('ALTER TABLE teacher DROP FOREIGN KEY FK_B0F6A6D5EE45BDBF');
        $this->addSql('ALTER TABLE course DROP FOREIGN KEY FK_169E6FB941807E1D');
        $this->addSql('DROP TABLE about_us');
        $this->addSql('DROP TABLE about_us_media_object');
        $this->addSql('DROP TABLE course');
        $this->addSql('DROP TABLE media_object');
        $this->addSql('DROP TABLE teacher');
        $this->addSql('DROP TABLE user_course');
        $this->addSql('ALTER TABLE user DROP telephone, DROP given_name, DROP family_name, DROP patronymic, DROP telegram_number, DROP age, DROP address');
    }
}
